package com.sel1;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class SimpleWebDriverMethods {

	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir") + "\\Drivers\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		driver.get("https://www.google.com/");
		//driver.get("https://www.facebook.com/");
		//driver.get("https://www.quora.com/");
		//driver.get("https://www.linkedin.com/");
		String currentURL=driver.getCurrentUrl();
		System.out.println("Current URL- "+currentURL);
		String title=driver.getTitle();
		System.out.println("Title- "+title);
		//String pageSource=driver.getPageSource();
		//System.out.println("Page source- "+pageSource);
	}
}
