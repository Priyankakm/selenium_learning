package com.sel3;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class Assignment3 {

	public static void main(String[] args) throws InterruptedException {
		System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir") + "\\Drivers\\chromedriver83.exe");
		WebDriver driver = new ChromeDriver();
		driver.get("https://paytm.com/");
		driver.manage().window().maximize();
		Thread.sleep(2000);
		WebElement searchBox = driver.findElement(By.cssSelector("input[type=\"search\"]"));
		searchBox.sendKeys("iPhone XR (64GB) � Yellow");
		searchBox.sendKeys(Keys.ENTER);
		Thread.sleep(2000);
		// //div[@class="_1fje"]//div[@class="_2i1r"]//a//div[@class="pCOS"]//span//span[@class="_1kMS"]
		List<WebElement> products = driver.findElements(By.xpath("//div[@class=\"_1fje\"]//div[@class=\"_2i1r\"]//a"));
		System.out.println("Number of Products- " + products.size());
		for (int i = 0; i < products.size(); i++) {
			System.out.println(products.get(i).getAttribute("title"));
		}
		List<WebElement> productPrice = driver.findElements(By.xpath("//div[@class=\"_1fje\"]//div[@class=\"_2i1r\"]//a//div[@class=\"pCOS\"]//span//span[@class=\"_1kMS\"]"));
		System.out.println("----------------------");
		for (int i = 0; i < productPrice.size(); i++) {
			System.out.println(productPrice.get(i).getText());
		}
	}
}
