package com.sel3;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class Assignment1 {

	public static void main(String[] args) throws InterruptedException {
		System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir") + "//Drivers//chromedriver83.exe");
		WebDriver driver = new ChromeDriver();
		driver.get("http://www.amazon.in/");
		driver.manage().window().maximize();
		//Search box
		driver.findElement(By.xpath("//*[@id=\"twotabsearchtextbox\"]")).sendKeys("iPhone XR (64GB) � Yellow");
		driver.findElement(By.xpath("//*[@type=\"submit\" and @value=\"Go\"]")).click();
		Thread.sleep(2000);
		//Mobile Price on Amazon
		String price1=driver.findElement(By.xpath("//span[@class=\"a-price-whole\"][1]")).getText();
		System.out.println("On Amazon- "+price1);
		Thread.sleep(3000);
		driver.get("https://www.flipkart.com/");
		driver.findElement(By.xpath("//button[@class=\"_2AkmmA _29YdH8\"]")).click();
		driver.findElement(By.xpath("//input[@name=\"q\"]")).sendKeys("iPhone XR (64GB) � Yellow");
		//Click the search button
		driver.findElement(By.xpath("//button[@type=\"submit\"]")).click();
		Thread.sleep(2000);
		//Mobile Price on Flipkart
		String text1=driver.findElement(By.xpath("//div[@class=\"_1vC4OE _2rQ-NK\"]")).getText();
		StringBuilder builder = new StringBuilder(text1);
		builder.deleteCharAt(0);
		String price2=builder.toString();
		System.out.println("On Flipkart- "+price2);
	}

}
