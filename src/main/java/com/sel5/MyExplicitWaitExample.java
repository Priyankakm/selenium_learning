package com.sel5;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class MyExplicitWaitExample {

	private WebDriverWait wait;
	public MyExplicitWaitExample(WebDriver driver, long timeOut, int poll) {
		this.wait = new WebDriverWait(driver, 3, 1000);	
	}
	public WebElement searchElement(By locator) {
		WebElement element = wait.withMessage("Could not find the slowly loading text")
				.until(ExpectedConditions.visibilityOfElementLocated(locator));
		return element;
	}
	public void typeToTextBox(By inputBox, String message) {
		wait.withMessage("Could not find the slowly loading text")
				.until(ExpectedConditions.visibilityOfElementLocated(inputBox)).sendKeys(message);
	}
	public void clickOnButton(By button) {
		wait.withMessage("Could not find the slowly loading text")
				.until(ExpectedConditions.elementToBeClickable(button)).click();
	}
	public String getText(By element) {
		String data = wait.withMessage("Could not find the slowly loading text")
				.until(ExpectedConditions.visibilityOfElementLocated(element)).getAttribute("data-fullname");
		return data;
	}
}
