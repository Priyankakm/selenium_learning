package com.sel5;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class Runner {

	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir") + "//Drivers//chromedriver83.exe");
		WebDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("https://www.univadis.co.uk/login");
		By usernameTextBox = By.id("_username");
		By passwordTextBox = By.id("_password");
		By loginButton = By.cssSelector("input[value=\"Login\"]");
		By afterLoginText = By.xpath("//li[@class=\"user-account__user\"]//span");
		MyExplicitWaitExample myExample = new MyExplicitWaitExample(driver, 3, 1000);
		/*myExample.searchElement(usernameTextBox).sendKeys("TestUK123@aptushealth.com");
		myExample.searchElement(passwordTextBox).sendKeys("test123");
		myExample.searchElement(loginButton).click();
		String name = myExample.searchElement(afterLoginText).getAttribute("data-fullname");
		System.out.println(name);*/
		
		myExample.typeToTextBox(usernameTextBox, "TestUK123@aptushealth.com");
		myExample.typeToTextBox(passwordTextBox, "test123");
		myExample.clickOnButton(loginButton);
		String result = myExample.getText(afterLoginText);
		System.out.println(result);
	}
}
