package com.sel4;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class DropDown {

	public static void main(String[] args) throws InterruptedException {
		System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir") + "//Drivers//chromedriver83.exe");
		WebDriver driver = new ChromeDriver();
		driver.get("http://139.59.91.96:5000/selenium-workbook/registration-form.html");
		WebElement emailBox=driver.findElement(By.name("email"));
		emailBox.sendKeys("test@mail.com");
		Thread.sleep(2000);
		WebElement passwordBox=driver.findElement(By.id("password"));
		passwordBox.sendKeys("password");
		Thread.sleep(2000);
		//Dropdown- Use select class 
		Select refDropDown = new Select(driver.findElement(By.name("hearAbout")));
		refDropDown.selectByIndex(2);
		Thread.sleep(2000);
		refDropDown.selectByIndex(1);
		
		Thread.sleep(2000);
		WebElement commuteBox=driver.findElement(By.xpath("//input[@name=\"contact\" and @value=\"phone\"]"));
		commuteBox.click();
		Thread.sleep(2000);
		Select productDropDown = new Select(driver.findElement(By.name("interest")));
		productDropDown.selectByIndex(0);
		Thread.sleep(2000);
		productDropDown.selectByVisibleText("One plus 7");
		Thread.sleep(2000);
		productDropDown.deselectByIndex(0);
		Thread.sleep(2000);
		productDropDown.selectByVisibleText("Iphone X");
		
		Thread.sleep(2000);
		WebElement termBox = driver.findElement(By.cssSelector("input[name=\"terms\"]"));
		termBox.click();
		Thread.sleep(2000);
		WebElement signUpButton = driver.findElement(By.cssSelector("button[type=\"submit\"]"));
		signUpButton.click();
		//By x-path :- //h1[contains(text(), "Thank You!")]
		WebElement welcomeMsgHeading = driver.findElement(By.tagName("h1"));
		System.out.println(welcomeMsgHeading.getText());
	}

}
