package com.sel4;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class ExplicitWaitExample {

	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir") + "//Drivers//chromedriver83.exe");
		WebDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("https://www.univadis.co.uk/login");
		WebDriverWait wait = new WebDriverWait(driver, 3, 100);
		By usernameTextBox = By.id("_username");
		wait.withMessage("Could not find the slowly loading text")
				.until(ExpectedConditions.visibilityOfElementLocated(usernameTextBox))
				.sendKeys("TestUK123@aptushealth.com");
		
		By passwordTextBox = By.id("_password");
		wait.withMessage("Could not find the slowly loading text")
				.until(ExpectedConditions.visibilityOfElementLocated(passwordTextBox))
				.sendKeys("test123");
		
		By loginButton = By.cssSelector("input[value=\"Login\"]");
		wait.withMessage("Could not find the slowly loading text")
				.until(ExpectedConditions.elementToBeClickable(loginButton))
				.click();
		
		By afterLoginText = By.xpath("//li[@class=\"user-account__user\"]//span");
		String name = wait.withMessage("Could not find the slowly loading text")
					.until(ExpectedConditions.visibilityOfElementLocated(afterLoginText))
					.getAttribute("data-fullname");
		System.out.println(name);
	}
}
