package com.sel4;

import java.util.Iterator;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class WindowHandle {

	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir") + "//Drivers//chromedriver83.exe");
		WebDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("http://139.59.91.96:5000/selenium-workbook/open-a-new-window.html");
		//a[target="new-window-name"]
		WebElement link1 = driver.findElement(By.cssSelector("a[target=\"new-window-name\"]"));
		link1.click();
		//TestUK123@aptushealth.com
		String currentHandle = driver.getWindowHandle();
		System.out.println(currentHandle);
		
		Set<String> allHandles = driver.getWindowHandles();
		Iterator<String> myIterator = allHandles.iterator();
		while (myIterator.hasNext()) { // If there is a new handle or data for processing
			String handle = myIterator.next(); // get the new data or handle
			 if (!(handle.equals(currentHandle))) {
				driver.switchTo().window(handle);  //mandatory
				WebElement newPage1 = driver.findElement(By.tagName("h1"));
				System.out.println(newPage1.getText());
				driver.close();
			}
		}
		driver.switchTo().window(currentHandle);  //mandatory to go back to the main page
		WebElement newPage1 = driver.findElement(By.tagName("h1"));
		System.out.println(newPage1.getText());
	}

}
