package com.sel4;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class ImplicitWaitExample {

	public static void main(String[] args) throws InterruptedException {
		System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir") + "//Drivers//chromedriver83.exe");
		WebDriver driver = new ChromeDriver();
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		driver.manage().window().maximize();
		driver.get("https://www.univadis.co.uk/login");
		System.out.println(System.currentTimeMillis());
		driver.findElement(By.id("_username")).sendKeys("TestUK123@aptushealth.com");
		System.out.println(System.currentTimeMillis());
		driver.findElement(By.id("_password")).sendKeys("test123");
		System.out.println(System.currentTimeMillis());
		driver.findElement(By.cssSelector("input[value=\"Login\"]")).click();
		System.out.println(System.currentTimeMillis());
		//Thread.sleep(4000);
		String name = driver.findElement(By.xpath("//li[@class=\"user-account__user\"]//span")).getAttribute("data-fullname");
		System.out.println(System.currentTimeMillis());
		System.out.println(name);
	}
}
