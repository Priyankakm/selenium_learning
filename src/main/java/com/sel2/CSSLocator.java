package com.sel2;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class CSSLocator {

	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir") + "//Drivers//chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		driver.get("http://139.59.91.96:5000/selenium-workbook/login.html");
		driver.findElement(By.cssSelector("input[id=\"userid\"]")).sendKeys("priyanka");
		driver.findElement(By.cssSelector("*[name=password]")).sendKeys("password");
	}

}
