package com.sel2;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class IdLocator {

	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir")+ "//Drivers//chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		driver.get("https://www.suvideals.ooo/MyAccount.action");
		WebElement emailField=driver.findElement(By.id("userHandle"));
		emailField.sendKeys("priyankamajukar1994@gmail.com");
		WebElement searchField=driver.findElement(By.id("suggest"));
		searchField.sendKeys("mobile");
		WebElement passwordField=driver.findElement(By.id("password"));
		passwordField.sendKeys("testpassword");
		WebElement loginButton=driver.findElement(By.xpath("//button[@class='btn btn-login']"));
		//xpath-//button=[text()=\"LOGIN"\]
		loginButton.click();
		
		driver.get("http://automationpractice.com/index.php?controller=contact");
		WebElement emailBox=driver.findElement(By.id("email"));
		emailBox.sendKeys("test@mail.com");
		WebElement refBox=driver.findElement(By.id("id_order"));
		refBox.sendKeys("Testing");
		WebElement searchBox=driver.findElement(By.name("search_query"));
		searchBox.sendKeys("clothes");
		WebElement messageBox=driver.findElement(By.name("message"));
		messageBox.sendKeys("Automation testing");
		WebElement button=driver.findElement(By.xpath("//span[contains(text(),'Send')]"));
		button.click();
	}
}
