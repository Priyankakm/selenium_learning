package com.sel2;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class XPathLocator2 {

	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir") + "//Drivers//chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		driver.get("http://139.59.91.96:5000/selenium-workbook/styled-elements.html");
		//String browserText=driver.findElement(By.xpath("//h1")).getText();
		String browserText=driver.findElement(By.xpath("//p[contains(text(),\"A visible paragraph.\")]")).getText();
		System.out.println(browserText);
	}
}
