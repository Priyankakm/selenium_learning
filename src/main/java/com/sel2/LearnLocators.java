package com.sel2;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class LearnLocators {

	public static void main(String[] args) throws InterruptedException {
		//Step 1
		System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir") + "//Drivers//chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		//Step 2
		driver.get("https://www.suvideals.ooo/MyAccount.action");
		//Step 3
		WebElement newAccountButton=driver.findElement(By.id("new-account-btn"));
		newAccountButton.click();
		Thread.sleep(4000);
		//Step 4
		WebElement newAccountNameBox=driver.findElement(By.name("userName"));
		newAccountNameBox.sendKeys("Priyanka");
		
		WebElement passwordBox=driver.findElement(By.name("password"));
		passwordBox.sendKeys("password");
	}

}
