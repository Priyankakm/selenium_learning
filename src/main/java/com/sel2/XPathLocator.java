package com.sel2;

import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class XPathLocator {

	public static void main(String[] args) throws InterruptedException {
		System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir") + "//Drivers//chromedriver83.exe");
		WebDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		/*Dimension windowSize = new Dimension(1000, 800);
		driver.manage().window().setSize(windowSize);*/
		driver.get("http://139.59.91.96:5000/selenium-workbook/login.html");
		Thread.sleep(2000);
		driver.navigate().to("https://www.google.com/");
		driver.navigate().to("https://www.facebook.com/");
		Thread.sleep(2000);
		driver.navigate().refresh();
		System.out.println(driver.getCurrentUrl());
		driver.navigate().back();
		System.out.println(driver.getCurrentUrl());
		Thread.sleep(2000);
		driver.navigate().back();
		System.out.println(driver.getCurrentUrl());
		Thread.sleep(2000);
		driver.navigate().forward();
		System.out.println(driver.getCurrentUrl());
		Thread.sleep(2000);
		driver.navigate().back();
		System.out.println(driver.getCurrentUrl());
		//driver.findElement(By.xpath("//*[@*=\"userid\" and @id=\"userid\" and @type=\"text\"]")).sendKeys("priyanka");
		driver.findElement(By.xpath("//div[@id=\"email-group\"]/input")).sendKeys("pkm");
		driver.findElement(By.name("password")).sendKeys("password");
		driver.close(); //closes the current tab which, webdriver is using
		driver.quit();
	}
}
