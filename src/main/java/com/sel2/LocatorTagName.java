package com.sel2;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class LocatorTagName {

	public static void main(String[] args) throws InterruptedException {
		//Step 1
		System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir") + "//Drivers//chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		//Step 2
		//driver.get("view-source:https://www.suvideals.ooo/MyAccount.action");
		driver.get("https://www.suvideals.ooo/MyAccount.action");
		WebElement linkTag=driver.findElement(By.tagName("a"));
		String text=linkTag.getText();
		System.out.println(text);
		//<input>- It takes the first input tag
		WebElement inputTag=driver.findElement(By.tagName("input"));
		inputTag.sendKeys("mobile search");
		
		Thread.sleep(3000);
		WebElement linkText=driver.findElement(By.linkText("Forgot Your Password?"));
		String text1=linkText.getText();
		System.out.println(text1);
		
		WebElement partialLinkText=driver.findElement(By.partialLinkText("For"));
		String text2=partialLinkText.getText();
		System.out.println(text2);
	}
}
