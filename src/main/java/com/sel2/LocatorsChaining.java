package com.sel2;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class LocatorsChaining {

	public static void main(String[] args) throws InterruptedException {
		//Step 1
		System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir") + "//Drivers//chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		//Step 2
		driver.get("https://www.suvideals.ooo/MyAccount.action");
		//Step 3
		WebElement newAccountButton=driver.findElement(By.id("new-account-btn"));
		newAccountButton.click();
		Thread.sleep(4000);
		//Step 4 -------Locate Registration form---------
		WebElement newAccountRegistrationForm=driver.findElement(By.name("registration-form"));
		//Step 5
		WebElement newAccountNameBox=newAccountRegistrationForm.findElement(By.name("userName"));
		newAccountNameBox.sendKeys("Priyanka");
		//Step 6
		WebElement passwordBox=newAccountRegistrationForm.findElement(By.name("password"));
		passwordBox.sendKeys("password");
	}

}
