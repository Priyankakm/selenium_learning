package com.sel6;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class HandlingFrames {

	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir") + "\\Drivers\\chromedriver83.exe");
		WebDriver driver = new ChromeDriver();
		driver.get("http://139.59.91.96:5000/selenium-workbook/wyswyg-editor.html");
		driver.switchTo().frame("editor_ifr");
		WebElement editBox = driver.findElement(By.xpath("//body[@id=\"tinymce\"]/p"));
		String text= editBox.getText();
		System.out.println(text);
		editBox.clear();
		editBox.sendKeys("Priyanka wrote it via eclipse!"+ Keys.BACK_SPACE + Keys.BACK_SPACE);
		driver.switchTo().defaultContent();
		String h1TagName=driver.findElement(By.tagName("h1")).getText();
		//add
		System.out.println(h1TagName);
	}
}
